# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/mica/Desktop/Examples/C++/OpenGL/src/glad.c" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/glad.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../external/imgui"
  "../external/imgui/backends"
  "../external/glad/include"
  "../external/glm-0.9.9.8"
  "../external"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/mica/Desktop/Examples/C++/OpenGL/external/imgui/backends/imgui_impl_glfw.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/external/imgui/backends/imgui_impl_glfw.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/external/imgui/backends/imgui_impl_opengl3.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/external/imgui/backends/imgui_impl_opengl3.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/external/imgui/imgui.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/external/imgui/imgui.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/external/imgui/imgui_demo.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/external/imgui/imgui_demo.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/external/imgui/imgui_draw.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/external/imgui/imgui_draw.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/external/imgui/imgui_tables.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/external/imgui/imgui_tables.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/external/imgui/imgui_widgets.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/external/imgui/imgui_widgets.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/src/App.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/App.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/src/Loader.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/Loader.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/src/ModelNode.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/ModelNode.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/src/Scene.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/Scene.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/src/ShaderProgram.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/ShaderProgram.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/src/StaticModel.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/StaticModel.cpp.o"
  "/home/mica/Desktop/Examples/C++/OpenGL/src/main.cpp" "/home/mica/Desktop/Examples/C++/OpenGL/build/CMakeFiles/app.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../external/imgui"
  "../external/imgui/backends"
  "../external/glad/include"
  "../external/glm-0.9.9.8"
  "../external"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
