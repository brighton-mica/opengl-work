#ifndef MICA_RENDERER_HPP
#define MICA_RENDERER_HPP

#include "Mesh.hpp"

#include <vector>

class Renderer {
private:
  std::vector<Mesh> default_bin;
public:
  Renderer() = default;

  void render();
};

#endif // MICA_RENDERER_HPP