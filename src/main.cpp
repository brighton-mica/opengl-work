#include "App.hpp"

int main()
{
  const uint32_t win_width  = 1000;
  const uint32_t win_height = 800;

  App app { win_width, win_height }; 
  app.run();
  
  return 0;
}