#ifndef MICA_STATIC_MODEL
#define MICA_STATIC_MODEL

#include "ModelNode.hpp"
#include "CullInfo.hpp"
#include "Loader.hpp"
#include "BoundingBox.hpp"

#include <vector>
#include <string>

class StaticModel
{
private:
  std::string filepath;
  std::vector<ModelNode> node_list;

  // TODO - make this a aabbox
  BBox bbox;

  friend bool process_gltf(StaticModel* model);
  friend bool process_obj (StaticModel* model);

public:
  StaticModel(const std::string filepath);

  bool init();
  void cull(CullInfo& cull_info) const;

  void set_bbox(BBox _bbox) { bbox = _bbox; }
  const BBox& get_bbox() const { return bbox; }

  void destroy();
};

#endif // MICE_STATIC_MODEL