#ifndef MICA_SCENE_HPP
#define MICA_SCENE_HPP

#include "StaticModel.hpp"
#include "CullInfo.hpp"

#include <vector>
#include <memory>

class Scene
{
private:
  std::vector<std::unique_ptr<StaticModel>> static_model_list;
public:
  Scene() = default;

  void clear();
  void cull(CullInfo& cull_info) const;
  void add_static_model(std::unique_ptr<StaticModel> static_model) { static_model_list.push_back(std::move(static_model)); }
};

#endif // MICA_SCENE_HPP