#include "Defines.hpp"
#include "StaticModel.hpp"
#include "Loader.hpp"

StaticModel::StaticModel(const std::string _filepath)
  : filepath{ _filepath }
  , bbox { }
{}

bool StaticModel::init()
{
  bool loaded = false;
  if (filepath.ends_with(".obj"))
    loaded = process_obj(this);
  else if (filepath.ends_with(".gltf"))
    loaded = process_gltf(this);
  else
  {
    std::cerr << "Unable to process file. Unrecognized file extension: " + filepath << '\n';
    loaded = false;
  }

  return loaded;
}

void StaticModel::cull(CullInfo& cull_info) const
{
  for (const ModelNode& node : node_list)
  {
    node.cull(cull_info);
  }
}


void StaticModel::destroy()
{
  for (ModelNode& node : node_list)
  {
    node.destroy();
  }
}