#ifndef MICA_BOUNDING_BOX
#define MICA_BOUNDING_BOX

#include <limits>

// Hardcore placeholder for now

struct BBox
{
    float x_min = std::numeric_limits<float>::max();
    float x_max = std::numeric_limits<float>::min();
    float y_min = std::numeric_limits<float>::max();
    float y_max = std::numeric_limits<float>::min();
    float z_min = std::numeric_limits<float>::max();
    float z_max = std::numeric_limits<float>::min();
};

#endif // MICA_BOUNDING_BOX