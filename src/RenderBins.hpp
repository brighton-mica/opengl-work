#ifndef MICA_RENDER_BINS_HPP
#define MICA_RENDER_BINS_HPP

#include "Mesh.hpp"

#include <vector>

struct RenderBins
{
    std::vector<Mesh> standard;
};

#endif // MICA_RENDER_BINS_HPP