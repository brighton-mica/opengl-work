#include "ModelNode.hpp"

void ModelNode::cull(CullInfo& cull_info) const
{
  assert(mesh.valid());
  cull_info.bins.standard.push_back(mesh);

  for (const ModelNode child : children_list)
  {
    child.cull(cull_info);
  }
}