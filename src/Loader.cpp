#include "Loader.hpp"

#include "Defines.hpp"
#include "StaticModel.hpp"
#include "ModelNode.hpp"
#include "Mesh.hpp"
#include "BoundingBox.hpp"

#include <glad/glad.h>

#define TINYGLTF_IMPLEMENTATION
#define TINYGLTF_USE_CPP14
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <tinygltf/tiny_gltf.h>

#define TINYOBJLOADER_IMPLEMENTATION
// #define TINYOBJLOADER_USE_MAPBOX_EARCUT
#include <tinyobjloader/tiny_obj_loader.h>

#include <unordered_map>
#include <string>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

namespace gltf
{
    static bool load_gltf(
        tinygltf::Model& model,
        const std::string& filepath,
        const bool load_ascii = true)
    {
        tinygltf::TinyGLTF loader;
        std::string err;
        std::string warn;

        const bool load_success = (load_ascii)
            ? loader.LoadASCIIFromFile(&model, &err, &warn, filepath)
            : loader.LoadBinaryFromFile(&model, &err, &warn, filepath);

        if (!warn.empty()) {
            std::cerr << "Filename: " + filepath + " " + warn;
        }

        if (!err.empty()) {
            std::cerr << "Filename: " + filepath + " " + err;
        }

        if (!load_success) {
            std::cerr << "Failed to parse glTF : " + filepath << '\n';
        }

        return load_success;
    }

    static std::unordered_map<int, GLuint> parse_buffers(
        const tinygltf::Model& model)
    {
        std::unordered_map<int, GLuint> vbo_lookup;

        for (size_t i = 0; i < model.bufferViews.size(); ++i) {
            const tinygltf::BufferView& buffer_view = model.bufferViews[i];
            const tinygltf::Buffer& buffer = model.buffers[buffer_view.buffer];

            if (buffer_view.target == 0) {
                EXIT("Buffer target is zero!");
            }

            GLuint vbo_handle;
            glGenBuffers(1, &vbo_handle);
            glBindBuffer(buffer_view.target, vbo_handle);
            glBufferData(buffer_view.target, buffer_view.byteLength,
                        &buffer.data.at(0) + buffer_view.byteOffset,
                        GL_STATIC_DRAW);

            vbo_lookup[i] = vbo_handle;
        }

        return vbo_lookup;
    }

    static Mesh parse_mesh(
        const tinygltf::Model& model,
        const tinygltf::Mesh& mesh,
        const std::unordered_map<int, GLuint>& vbo_lookup)
    {
        GLuint vao_handle;
        glGenVertexArrays(1, &vao_handle);
        glBindVertexArray(vao_handle);

        Mesh new_mesh;
        new_mesh.set_vao_handle(vao_handle);

        for (size_t i = 0; i < mesh.primitives.size(); ++i)
        {
            const auto& primitive = mesh.primitives[i];
            const auto& index_accessor = model.accessors[primitive.indices];
            const auto& index_buffer_view = model.bufferViews[index_accessor.bufferView];
            glBindBuffer(index_buffer_view.target, vbo_lookup.at(index_accessor.bufferView));

            new_mesh.set_mode(primitive.mode);
            new_mesh.set_count(index_accessor.count);
            new_mesh.set_type(index_accessor.componentType);

            for (const auto& [name, accessor_idx] : primitive.attributes)
            {
                const auto& accessor = model.accessors[accessor_idx];
                const auto& buffer_view = model.bufferViews[accessor.bufferView];
                const auto& buffer = model.buffers[buffer_view.buffer];

                if (buffer_view.target == 0) { EXIT("No support for glDrawArrays yet!"); }


                int byteStride = accessor.ByteStride(model.bufferViews[accessor.bufferView]);
                assert(byteStride != -1);

                int size = 1;
                if (accessor.type != TINYGLTF_TYPE_SCALAR) { size = accessor.type; }

                GLuint attrib_idx = -1;
                if      (strcmp("POSITION", name.c_str()) == 0) attrib_idx = 0;
                else if (strcmp("NORMAL"  , name.c_str()) == 0) attrib_idx = 1;
                else if (strcmp("POSITION", name.c_str()) == 0) attrib_idx = 2;
                else std::cout << "Warning: Unsupported vertex attrib name " << name << '\n'; // EXIT("Unsupported vertex attrib name " + name);

                if (attrib_idx != -1)
                {
                    glBindBuffer(buffer_view.target, vbo_lookup.at(accessor.bufferView));
                    glEnableVertexAttribArray(attrib_idx);
                    glVertexAttribPointer(attrib_idx, size, accessor.componentType, 
                    accessor.normalized ? GL_TRUE : GL_FALSE,byteStride, 
                    BUFFER_OFFSET(accessor.byteOffset));
                }
            }
        }

        return new_mesh;
    }

    static ModelNode parse_node(
        const tinygltf::Model& tgltf_model,
        const tinygltf::Node& tgltf_node,
        const std::unordered_map<int, GLuint>& vbo_lookup)
    {
        ModelNode node { tgltf_node.name };

        if (tgltf_node.translation.size() > 0)
        node.translate(tgltf_node.translation[0], tgltf_node.translation[1], tgltf_node.translation[2]);

        // Process Mesh
        if ((tgltf_node.mesh >= 0) && (tgltf_node.mesh < tgltf_model.meshes.size()))
        {
            Mesh mesh = parse_mesh(tgltf_model, tgltf_model.meshes[tgltf_node.mesh], vbo_lookup);
            node.add_mesh(mesh);
            node.update_mesh_transformation();
        }

        // Process Children (also nodes)
        for (const auto& child_idx : tgltf_node.children)
        {
            ModelNode child_node = parse_node(tgltf_model, tgltf_model.nodes[child_idx], vbo_lookup);
            node.add_child(child_node);
        }

        return node;
    }

    static void dbg_model(tinygltf::Model &model)
    {
        for (auto &mesh : model.meshes) {
            std::cout << "mesh : " << mesh.name << std::endl;
            for (auto &primitive : mesh.primitives) {
            const tinygltf::Accessor &indexAccessor =
                model.accessors[primitive.indices];

            std::cout << "indexaccessor: count " << indexAccessor.count << ", type "
                        << indexAccessor.componentType << std::endl;

            // tinygltf::Material &mat = model.materials[primitive.material];
            // for (auto &mats : mat.values) {
            //   std::cout << "mat : " << mats.first.c_str() << std::endl;
            // }

            // for (auto &image : model.images) {
            //   std::cout << "image name : " << image.uri << std::endl;
            //   std::cout << "  size : " << image.image.size() << std::endl;
            //   std::cout << "  w/h : " << image.width << "/" << image.height
            //             << std::endl;
            // }

            std::cout << "indices : " << primitive.indices << std::endl;
            std::cout << "mode     : "
                        << "(" << primitive.mode << ")" << std::endl;

            for (auto &attrib : primitive.attributes) {
                std::cout << "attribute : " << attrib.first.c_str() << std::endl;
            }
            }
        }
    }
}

bool process_gltf(StaticModel* model)
{
    tinygltf::Model tgltf_model;
    if (gltf::load_gltf(tgltf_model, model->filepath) == false)
    {
        std::cerr << "Failed to load gltf file: " + model->filepath << '\n';
        return false;
    }

    const std::unordered_map<int, GLuint> vbo_lookup =
        gltf::parse_buffers(tgltf_model);

    const tinygltf::Scene& scene = tgltf_model.scenes[tgltf_model.defaultScene];
    for (size_t i = 0; i < scene.nodes.size(); ++i)
    {
        assert((scene.nodes[i] >=0) && scene.nodes[i] < tgltf_model.nodes.size());

        ModelNode node = gltf::parse_node(tgltf_model, tgltf_model.nodes[scene.nodes[i]], vbo_lookup);
        model->node_list.push_back(node);
    }

    // for (auto [buffer_view_idx, vbo_handle] : vbo_lookup)
    // {
    //   glDeleteBuffers(1, &vbo_handle);
    // }

    return true;
}

namespace obj
{
    struct MeshInfo
    {
        Mesh mesh;
        GLuint vbo_handle;
        GLuint ebo_handle;
    };

    static MeshInfo parse_mesh(
        const tinyobj::shape_t& shape,
        const tinyobj::attrib_t& attribs,
        BBox& bbox)
    {
        struct Vertex
        {
            float pos[3]    = { 0.0f, 0.0f, 0.0f };
            // float normal[3] = { 0.0f, 0.0f, 0.0f };
            // float uv[2]     = { 0.0f, 0.0f };
        };

        uint64_t index_count = 0;
        std::map<std::string, uint64_t> index_lookup {};

        std::vector<GLuint> indices  {};
        std::vector<Vertex> vertices {};

        // Loop over faces(polygon)
        size_t index_offset = 0;
        for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); f++)
        {
            size_t fv = size_t(shape.mesh.num_face_vertices[f]);

            std::string hash;

            // Loop over vertices in the face.
            for (size_t v = 0; v < fv; v++) 
            {
                tinyobj::index_t idx = shape.mesh.indices[index_offset + v];

                const std::string hash = std::to_string(idx.vertex_index); // +
                    // std::to_string(idx.normal_index) + 
                    // std::to_string(idx.texcoord_index);

                auto iter = index_lookup.find(hash);
                if (iter == index_lookup.end())
                {
                    index_lookup[hash] = index_count;
                    indices.push_back(index_count++);
                }
                else
                {
                    indices.push_back(index_lookup.at(hash));
                    continue;
                }
            
                Vertex vertex {};

                // access to vertex
                vertex.pos[0] = attribs.vertices[3*size_t(idx.vertex_index)+0];
                vertex.pos[1] = attribs.vertices[3*size_t(idx.vertex_index)+1];
                vertex.pos[2] = attribs.vertices[3*size_t(idx.vertex_index)+2];

                if (vertex.pos[0] < bbox.x_min)
                    bbox.x_min = vertex.pos[0];
                if (vertex.pos[0] > bbox.x_max)
                    bbox.x_max = vertex.pos[0];
                if (vertex.pos[1] < bbox.y_min)
                    bbox.y_min = vertex.pos[1];
                if (vertex.pos[1] > bbox.y_max)
                    bbox.y_max = vertex.pos[1];
                if (vertex.pos[2] < bbox.z_min)
                    bbox.z_min = vertex.pos[2];
                if (vertex.pos[2] > bbox.z_max)
                    bbox.z_max = vertex.pos[2];

                // // Check if `normal_index` is zero or positive. negative = no normal data
                // if (idx.normal_index >= 0)
                // {
                //     vertex.normal[0] = attribs.normals[3*size_t(idx.normal_index)+0];
                //     vertex.normal[1] = attribs.normals[3*size_t(idx.normal_index)+1];
                //     vertex.normal[2] = attribs.normals[3*size_t(idx.normal_index)+2];
                // }

                // // Check if `texcoord_index` is zero or positive. negative = no texcoord data
                // if (idx.texcoord_index >= 0)
                // {
                //     vertex.uv[0] = attribs.texcoords[2*size_t(idx.texcoord_index)+0];
                //     vertex.uv[1] = attribs.texcoords[2*size_t(idx.texcoord_index)+1];
                // }

                vertices.push_back(vertex);
            }
            index_offset += fv;
        }

        // for (int i = 0; i < indices.size(); ++i)
        // {
        //     if (i%3 == 0)
        //         std::cout << '\n';
        //     std::cout << "Idx: " << i << " Indicie: " << indices[i] << " Vert: ";
        //     for (int j = 0; j < 3; ++j)
        //         std::cout << vertices[indices[i]].pos[j] << " ";
        //     std::cout << '\n';
        // }

        GLuint vao_handle, vbo_handle, ebo_handle;
        glGenVertexArrays(1, &vao_handle);
        glGenBuffers(1, &vbo_handle);
        glGenBuffers(1, &ebo_handle);

        Mesh new_mesh;
        new_mesh.set_vao_handle(vao_handle);
        new_mesh.set_mode(GL_TRIANGLES);
        new_mesh.set_type(GL_UNSIGNED_INT);
        new_mesh.set_count((GLsizei)indices.size());

        glBindVertexArray(vao_handle);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_handle);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex),
                    (void*)(vertices.data()), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_handle);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), (void*)indices.data(),
                     GL_STATIC_DRAW);
        
        glEnableVertexAttribArray(0u);
        glVertexAttribPointer(0u, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                            (void*)offsetof(Vertex, pos));
        // glEnableVertexAttribArray(1u);
        // glVertexAttribPointer(1u, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
        //                     (void*)offsetof(Vertex, normal));
        // glEnableVertexAttribArray(2u);
        // glVertexAttribPointer(2u, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
        //                     (void*)offsetof(Vertex, uv));

        glBindVertexArray(0u);
        // glDeleteBuffers(1, &vao_handle);
        // glDeleteBuffers(1, &ebo_handle);

        // TODO - delete these buffers

        return { new_mesh, vbo_handle, ebo_handle };
    }

    static ModelNode parse_node(const tinyobj::shape_t& shape, const tinyobj::attrib_t& attribs)
    {
        ModelNode node { "" };

        BBox bbox {};
        MeshInfo mesh_info = parse_mesh(shape, attribs, bbox);
        node.add_mesh(mesh_info.mesh);
        node.set_vbo(mesh_info.vbo_handle);
        node.set_ebo(mesh_info.ebo_handle);
        node.set_bbox(bbox);
        node.update_mesh_transformation();

        return node;
    }
}

bool process_obj(StaticModel* model)
{
    tinyobj::ObjReaderConfig reader_config;
    tinyobj::ObjReader reader;

    if (!reader.ParseFromFile(model->filepath, reader_config))
    {
        std::cerr << "Failed to open .obj file (" + model->filepath + "): " + reader.Error();
        return false;
    }

    if (!reader.Warning().empty())
        std::cout << "TinyObjReader: " << reader.Warning();

    auto& attrib = reader.GetAttrib();
    auto& shapes = reader.GetShapes();

    BBox bbox {};

    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++)
    {
        ModelNode node = obj::parse_node(shapes[s], attrib);
        model->node_list.push_back(node);

        const BBox& node_bbox = node.get_bbox();

        if (node_bbox.x_min < bbox.x_min)
            bbox.x_min = node_bbox.x_min;
        if (node_bbox.x_max > bbox.x_max)
            bbox.x_max = node_bbox.x_max;
        if (node_bbox.y_min < bbox.y_min)
            bbox.y_min = node_bbox.y_min;
        if (node_bbox.y_max > bbox.y_max)
            bbox.y_max = node_bbox.y_max;
        if (node_bbox.z_min < bbox.z_min)
            bbox.z_min = node_bbox.z_min;
        if (node_bbox.z_max > bbox.z_max)
            bbox.z_max = node_bbox.z_max;
    }

    model->set_bbox(bbox);

    return true;
}