#include "Scene.hpp"


void Scene::cull(CullInfo& cull_info) const
{
    for (const auto& model : static_model_list)
    {
        model->cull(cull_info);
    }
}

void Scene::clear()
{
    for (auto& model : static_model_list)
        model->destroy();
    static_model_list.clear();
}