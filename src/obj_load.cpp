


// struct Vertex {
//     glm::vec3 position;
//     glm::vec3 normal;
//     glm::vec2 texcoord;
// };


// struct PerFrameData {

// };

// struct PerMaterialData {

// };

// struct PerDrawData {
//     glm::mat4& model_matrix; // owned by model not renderable

//     PerDrawData(glm::mat4& parernt_matrix)
//         : model_matrix { parernt_matrix }
//     {}
// };

// struct Transform
// {
//     Transform() = default;

//     void rotate(const float degrees, const glm::vec3&& axis)
//     {
//         matrix = glm::rotate(matrix, glm::radians(degrees), axis);
//     }

//     void scale();
//     void translate(const glm::vec3&& translation)
//     {
//         matrix = glm::translate(matrix, translation);
//     }

//     glm::mat4 matrix = glm::mat4 { 1.0f };
// };

// struct Renderable
// {
//     const std::vector<Vertex> vertices;
//     GLuint gl_vao_handle, gl_vbo_handle;
//     PerDrawData per_draw_data;

//     void init()
//     {
//         glGenVertexArrays(1, &gl_vao_handle);
//         glGenBuffers(1, &gl_vbo_handle);

//         glBindVertexArray(gl_vao_handle);
//         glBindBuffer(GL_ARRAY_BUFFER, gl_vbo_handle);

//         glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex),
//                     (void*)(vertices.data()), GL_STATIC_DRAW);
        
//         glEnableVertexAttribArray(0u);
//         glVertexAttribPointer(0u, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
//                             (void*)offsetof(Vertex, position));

//         glEnableVertexAttribArray(1u);
//         glVertexAttribPointer(1u, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
//                             (void*)offsetof(Vertex, normal));

//         glEnableVertexAttribArray(2u);
//         glVertexAttribPointer(2u, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
//                             (void*)offsetof(Vertex, texcoord));
        
//         glBindBuffer(GL_ARRAY_BUFFER, 0u);
//         glBindVertexArray(0u);
//     }

// public:
//     Renderable(const std::vector<Vertex>& _vertices, glm::mat4& parent_matrix)
//         : vertices(std::move(_vertices))
//         , per_draw_data { parent_matrix }
//     { init(); }

//     void free()
//     {
//         glDeleteVertexArrays(1, &gl_vao_handle);
//         glDeleteBuffers(1, &gl_vbo_handle);
//     }

//     void set_draw_state(const ShaderProgram& shader_program) const
//     {
//         shader_program.set_uni_mat4("u_model_mat", per_draw_data.model_matrix);
//     }

//     void render() const
//     {
//         glBindVertexArray(gl_vao_handle);
//         glDrawArrays(GL_TRIANGLES, 0, vertices.size());
//         glBindVertexArray(0u);
//     }
// };

// class Model
// {
// private:
//     std::vector<Renderable> renderables;

//     static std::vector<Vertex> create_renderable(const tinyobj::shape_t& shape,
//         const tinyobj::attrib_t& attribs,
//         const std::vector<tinyobj::material_t>& materials);

//     void parse_obj_file(const std::string& filepath);
// public:
//     Model(const std::string& filepath);
//     ~Model();

//     void render(const ShaderProgram& shader_program) const;

//     Transform model_transform;
// };

// Model::Model(const std::string& filepath)
// {
//     parse_obj_file(filepath);
// }

// Model::~Model()
// {
//     for (Renderable& renderable : renderables)
//     {
//         renderable.free();
//     }
// }

// void Model::render(const ShaderProgram& shader_program) const
// {
//     for (const Renderable& renderable : renderables)
//     {
//         renderable.set_draw_state(shader_program);
//         renderable.render();
//     }
// }

// void Model::parse_obj_file(const std::string& filepath)
// {
//     tinyobj::ObjReaderConfig reader_config;
//     reader_config.mtl_search_path = "./"; // Path to material files

//     tinyobj::ObjReader reader;

//     if (!reader.ParseFromFile(filepath, reader_config))
//     {
//         if (!reader.Error().empty())
//             std::cerr << "TinyObjReader: " << reader.Error();
//         exit(1);
//     }

//     if (!reader.Warning().empty())
//         std::cout << "TinyObjReader: " << reader.Warning();

//     auto& attrib = reader.GetAttrib();
//     auto& shapes = reader.GetShapes();
//     auto& materials = reader.GetMaterials();

//     renderables.reserve(shapes.size());

//     for (size_t i = 0; i < shapes.size(); ++i)
//     {
//         renderables.emplace_back(
//             create_renderable(shapes[i], attrib, materials),
//             model_transform.matrix);
//     }
// }

// std::vector<Vertex> Model::create_renderable(const tinyobj::shape_t& shape, 
//     const tinyobj::attrib_t& attrib,
//     const std::vector<tinyobj::material_t>& materials)
// {
//     size_t index_offset = 0;
//     std::vector<Vertex> vertices;
//     vertices.reserve(shape.mesh.num_face_vertices.size() * 3);

//     // For every face in shape...
//     for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); f++)
//     {
//         const size_t fv = size_t(shape.mesh.num_face_vertices[f]);
//         assert(fv == 3);

//         // For each vertex in face... (assuming triangle topology)
//         for (size_t v = 0; v < fv; v++)
//         {
//             // Access (vertex_index, normal_index, texcoord_index) for this face
//             const tinyobj::index_t idx = shape.mesh.indices[index_offset + v];

//             Vertex vertex {};
//             vertex.position.x = attrib.vertices[3*size_t(idx.vertex_index)+0];
//             vertex.position.y = attrib.vertices[3*size_t(idx.vertex_index)+1];
//             vertex.position.z = attrib.vertices[3*size_t(idx.vertex_index)+2];

//             // Check if `normal_index` is zero or positive. negative = no normal data
//             if (idx.normal_index >= 0)
//             {
//                 vertex.normal.x = attrib.normals[3*size_t(idx.normal_index)+0];
//                 vertex.normal.y = attrib.normals[3*size_t(idx.normal_index)+1];
//                 vertex.normal.z = attrib.normals[3*size_t(idx.normal_index)+2];
//             }
//             else
//             {
//                 EXIT("Model has no normal data!");
//             }

//             // Check if `texcoord_index` is zero or positive. negative = no texcoord data
//             if (idx.texcoord_index >= 0)
//             {
//                 vertex.texcoord.x = attrib.texcoords[2*size_t(idx.texcoord_index)+0];
//                 vertex.texcoord.y = attrib.texcoords[2*size_t(idx.texcoord_index)+1];
//             }
//             else
//             {
//                 EXIT("Model has no texcoord data!");
//             }

//             vertices.push_back(vertex);
//         }
//         index_offset += fv;

//         // per-face material
//         // shapes[s].mesh.material_ids[f];
//     }

//     return vertices;
// }
