#ifndef MICA_LOADER
#define MICA_LOADER

class StaticModel;

bool process_gltf(StaticModel* model); 
bool process_obj (StaticModel* model); 

#endif // MICA_LOADER