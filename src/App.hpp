#ifndef APP_HPP
#define APP_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stdint.h>
#include <functional>
#include <iostream>

class GLFWwindow;

struct Stats
{
    double frame_time_ms;
};

class Camera
{
private:
    glm::mat4 matrix { 1.0f };
    glm::vec3 world_position { 0.0f, 0.0f, 0.0f  };
    glm::vec3 forward_vec { 0.0f, 0.0f, -1.0f };
    glm::vec3 m_up { 0.0f, 1.0f, 0.0f  };
    glm::vec3 m_right { 1.0f, 0.0f, 0.0f };

    float pitch = 0.0f;
    float yaw   = -1.0f * glm::half_pi<float>();

    void update_vectors()
    {
        glm::vec3 front;
        front.x = cos(yaw) * cos(pitch);
        front.y = sin(pitch);
        front.z = sin(yaw) * cos(pitch);
        forward_vec = glm::normalize(front);

        m_right = glm::normalize(glm::cross(forward_vec, m_up));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
        m_up    = glm::normalize(glm::cross(m_right, forward_vec));
    }

public:
    Camera()
    {
        update_vectors();
    }

    void reset_rotation()
    {
        pitch = 0.0f;
        yaw   = -1.0f * glm::half_pi<float>();
        forward_vec = { 0.0f, 0.0f, -1.0f };
        m_up = { 0.0f, 1.0f, 0.0f };
        m_right = { 1.0f, 0.0f, 0.0f };
    }

    void translate_to(const glm::vec3 translation)
    {
        world_position = translation;
    }

    void translate_x_by(const float translation) { world_position += m_right * translation; }
    void translate_y_by(const float translation) { world_position += m_up * translation; }
    void translate_z_by(const float translation) { world_position += forward_vec * translation; }

    void rotate_x_to(float rad)
    {
        pitch = rad;
        update_vectors();
    }

    void rotate_y_to(const float rad)
    {
        // yaw = degree - 90.0f;
        yaw = rad - glm::half_pi<float>();
        update_vectors();
    }

    void rotate_z_to(const float rad)
    {
        float roll = rad;
        m_up = { -sin(roll), cos(roll), m_up.z }; 
        m_up = glm::normalize(m_up);
        update_vectors();
    }

    glm::mat4 get_view_matrix()
    {
        return glm::lookAt(world_position, world_position + forward_vec, m_up);
    }
};

#include "Scene.hpp"
#include "BoundingBox.hpp"

class App
{
private:
    // Renderer m_renderer;
    Scene m_scene;
    Camera m_camera;
    Stats stats;

    uint32_t m_win_width;
    uint32_t m_win_height;
    GLFWwindow* m_window;


    glm::mat4 m_view_matrix = glm::mat4 { 1.0f };
    glm::mat4 m_proj_matrix = glm::mat4 { 1.0f };

public:
    App(const uint32_t win_width, const uint32_t win_height);
    ~App();

    void render_bof();
    void render();
    void render_eof();

    void run();
    void center_camera(const BBox& bbox);
};

#endif // APP_HPP