#ifndef MICA_CULL_INFO
#define MICA_CULL_INFO

#include "RenderBins.hpp"

struct CullInfo
{
  RenderBins& bins;
  // eyepoint frustum

  CullInfo(RenderBins& _bins)
    : bins { _bins }
  {}
};

#endif // MICA_CULL_INFO