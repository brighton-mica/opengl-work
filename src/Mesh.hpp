#ifndef MICA_MESH_HPP
#define MICA_MESH_HPP

#include "ShaderProgram.hpp"

#include <glad/glad.h>
#include <glm/mat4x4.hpp>

class ModelNode;

struct PerDrawData
{
  glm::mat4 model_matrix = glm::mat4 { 1.0f };
};

struct Mesh
{
private:
  GLuint vao_handle = -1;
  GLenum mode = GL_TRIANGLES;
  GLsizei count = -1;
  GLenum type = GL_UNSIGNED_BYTE;

  PerDrawData per_draw_data;

public:
  Mesh() = default;

  inline void set_draw_state(const ShaderProgram& shader_program) const noexcept
  { shader_program.set_uni_mat4("u_model_mat", per_draw_data.model_matrix); }

  inline void render() const // noexcept
  {
    glBindVertexArray(vao_handle);
    glDrawElements(mode, count, type, 0);
  }

  void set_vao_handle(const GLuint handle) { vao_handle = handle; }
  void set_mode(const GLenum _mode) { mode = _mode; }
  void set_count(const GLsizei _count) { count = _count; }
  void set_type(const GLenum _type) { type = _type; }

  bool valid() const noexcept { return (vao_handle != -1); }

  void destroy()
  {
    glDeleteVertexArrays(1, &vao_handle);
  }

  friend ModelNode;
};

#endif // MICA_MESH_HPP