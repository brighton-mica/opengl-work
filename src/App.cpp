#include "Defines.hpp"

#include "App.hpp"
#include "ShaderProgram.hpp"
#include "StaticModel.hpp"
#include "CullInfo.hpp"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <chrono>
#include <memory>

// Put these here becuase I'm lazy
// TODO - find local spot
static constexpr float gFov = 45.0f;
static glm::vec3 gCurrEyeReset { 0.0f, 0.0f, 0.0f };

App::App(const uint32_t win_width, const uint32_t win_height)
    : m_win_width(win_width)
    , m_win_height(win_height)
    , m_scene { }
    , m_camera { }
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    m_window = glfwCreateWindow(static_cast<int>(m_win_width), static_cast<int>(m_win_height), "LearnOpenGL", NULL, NULL);
    if (m_window == NULL)
    {
        glfwTerminate();
        EXIT("Failed to create glfw window!");
    }
    glfwMakeContextCurrent(m_window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        EXIT("Failed to initialize GLAD!");
    }  

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(m_window, true);
    ImGui_ImplOpenGL3_Init("#version 450");
}

App::~App()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(m_window);
    glfwTerminate();
}

void App::render_bof()
{
    static auto old_time = std::chrono::high_resolution_clock::now();
    auto new_time = std::chrono::high_resolution_clock::now();
    stats.frame_time_ms = 0.001 * std::chrono::duration_cast<std::chrono::microseconds>(new_time - old_time).count();
    old_time = std::chrono::high_resolution_clock::now();

    glClearColor(0.12f, 0.68f, 0.87f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void App::render_eof()
{
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(m_window);
    glfwPollEvents();
}

void App::center_camera(const BBox& bbox)
{
    const glm::vec3 bbox_center { ((bbox.x_min + bbox.x_max) / 2.0f), 
                                  ((bbox.y_min + bbox.y_max) / 2.0f),
                                  ((bbox.z_min + bbox.z_max) / 2.0f) };

    glm::vec3 eye_pos { bbox_center.x, bbox_center.y, bbox.z_max };
    const float half_x = glm::distance(bbox_center.x, bbox.x_max);
    const float half_y = glm::distance(bbox_center.y, bbox.y_max);

    // Use 2.2 instead of 2.0 to give padding
    eye_pos.z += (half_x > half_y) 
        ? (half_x / glm::tan(glm::radians(gFov / 2.1f)))
        : (half_y / glm::tan(glm::radians(gFov / 2.1f)));

    m_camera.reset_rotation();
    m_camera.translate_to(eye_pos);
    gCurrEyeReset = eye_pos;
}

void App::run()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    const float frustum_padding = 1.0f;

    ShaderProgram default_shader_program(
        "/home/mica/Desktop/Examples/C++/OpenGL/glsl/shader.vert",
        "/home/mica/Desktop/Examples/C++/OpenGL/glsl/shader.frag",
        "default-shader-program");

    // Projection init
    float near_clip_plane = 1.0f;
    float far_clip_plane  = 1000.0f;
    m_proj_matrix = glm::perspective(
        glm::radians(gFov), static_cast<float>(m_win_width) / m_win_height,
        near_clip_plane, far_clip_plane);

    // Model init
    std::unique_ptr<StaticModel> model = std::make_unique<StaticModel>("../assets/obj/rock.obj");
    model->init();
    center_camera( model->get_bbox() );

    // Config init
    glm::vec3 color { 0.12f, 0.83f, 0.32f };

    // Scene init
    m_scene.add_static_model( std::move(model) );

    while(!glfwWindowShouldClose(m_window))
    {
        render_bof();

        // Cull + Render
        {
            // Basic Cull (everything passes)
            RenderBins bins {};
            CullInfo cull_info { bins }; 
            m_scene.cull(cull_info);

            // Basic Render
            default_shader_program.use();
            {
                default_shader_program.set_uni_mat4("u_view_mat", m_camera.get_view_matrix());
                default_shader_program.set_uni_mat4("u_proj_mat", m_proj_matrix);
                default_shader_program.set_uni_mat4("u_model_mat", glm::mat4(1.f));
                default_shader_program.set_uni_vec3("u_color", color);

                for (const Mesh& mesh : cull_info.bins.standard)
                {
                    mesh.set_draw_state(default_shader_program);
                    mesh.render();
                }
            }
        }

        // Gui
        {
            if (ImGui::Begin("Gui"))
            {
                ImGui::Text("Frame Time: %.3f ms", stats.frame_time_ms);
                static glm::vec3 curr_camera_rotation = glm::vec3 { 0.0f };

                if (ImGui::CollapsingHeader("Asset"))
                {
                    ImGui::Text("Color:"); ImGui::SameLine(75);
                    ImGui::ColorEdit3("##color", (float*)&color);

                    static char buf[128] = "";
                    ImGui::Text("Filepath:"); ImGui::SameLine(75);
                    ImGui::InputText("##model-filepath", buf, 128);
                    if (ImGui::Button("Load Model"))
                    {
                        const std::string str_buff = buf;
                        std::unique_ptr<StaticModel> new_model = std::make_unique<StaticModel>(str_buff); 

                        if (new_model->init())
                        {
                            // Adjust eyepoint
                            const BBox& new_bbox = new_model->get_bbox();
                            center_camera( new_model->get_bbox() );

                            m_scene.clear();
                            m_scene.add_static_model( std::move(new_model) );

                            curr_camera_rotation[0] = 0.0f;
                            curr_camera_rotation[1] = 0.0f;
                            curr_camera_rotation[2] = 0.0f;
                        }
                    }
                }

                if (ImGui::CollapsingHeader("Camera"))
                {
                    static float step = 1.0f;
                    float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
                    float spacing_factor = 3.0f;

                    ImGui::Text("Camera Translation");
                    ImGui::Text("x");
                    ImGui::SameLine(0.0f, spacing * spacing_factor);
                    ImGui::PushButtonRepeat(true);
                    if (ImGui::ArrowButton("##-x", ImGuiDir_Left )) { m_camera.translate_x_by(-step); }
                    ImGui::SameLine(0.0f, spacing);
                    if (ImGui::ArrowButton("##+x", ImGuiDir_Right)) { m_camera.translate_x_by(step); }
                    ImGui::PopButtonRepeat();

                    ImGui::SameLine(0.0f, spacing * spacing_factor);
                    ImGui::Text("y");
                    ImGui::SameLine(0.0f, spacing * spacing_factor);
                    ImGui::PushButtonRepeat(true);
                    if (ImGui::ArrowButton("##-y", ImGuiDir_Down)) { m_camera.translate_y_by(-step); }
                    ImGui::SameLine(0.0f, spacing);
                    if (ImGui::ArrowButton("##+y", ImGuiDir_Up  )) { m_camera.translate_y_by(step); }
                    ImGui::PopButtonRepeat();

                    ImGui::SameLine(0.0f, spacing * spacing_factor);
                    ImGui::Text("z");
                    ImGui::SameLine(0.0f, spacing * spacing_factor);
                    ImGui::PushButtonRepeat(true);
                    if (ImGui::ArrowButton("##-z", ImGuiDir_Down)) { m_camera.translate_z_by(-step); }
                    ImGui::SameLine(0.0f, spacing);
                    if (ImGui::ArrowButton("##+z", ImGuiDir_Up  )) { m_camera.translate_z_by(step); }
                    ImGui::PopButtonRepeat();

                    ImGui::Text("Camera Rotation");

                    ImGui::Text("x: "); ImGui::SameLine();
                    if (ImGui::SliderAngle("##x-angle", &curr_camera_rotation.x, -89.9f, 89.9))
                    {
                        m_camera.rotate_x_to(curr_camera_rotation.x);
                    }
                    ImGui::Text("y: "); ImGui::SameLine();
                    if (ImGui::SliderAngle("##y-angle", &curr_camera_rotation.y))
                    {
                        m_camera.rotate_y_to(-curr_camera_rotation.y);
                    }
                    ImGui::Text("z: "); ImGui::SameLine();
                    if (ImGui::SliderAngle("##z-angle", &curr_camera_rotation.z))
                    {
                        m_camera.rotate_z_to(curr_camera_rotation.z);
                    }

                    if (ImGui::Button("Reset"))
                    {
                        m_camera.reset_rotation();
                        m_camera.translate_to(gCurrEyeReset);

                        curr_camera_rotation[0] = 0.0f;
                        curr_camera_rotation[1] = 0.0f;
                        curr_camera_rotation[2] = 0.0f;
                    }
                }

                if (ImGui::CollapsingHeader("Config"))
                {
                    ImGui::Text("Clip Planes:"); ImGui::SameLine();
                    if (ImGui::DragFloatRange2("##clip-planes", &near_clip_plane, &far_clip_plane,
                        0.1f, 0.01f, 50.0f, "Near: %.2f", "Far: %.2f",
                        ImGuiSliderFlags_AlwaysClamp))
                    {
                        m_proj_matrix = glm::perspective(glm::radians(45.0f),
                            static_cast<float>(m_win_width) / m_win_height,
                            near_clip_plane, far_clip_plane);
                    }

                    static const char* items[] = { "Fill", "Line", "Point" };
                    static int curr_poly_mode = 0;
                    ImGui::Text("Polygon Mode:"); ImGui::SameLine();
                    ImGui::RadioButton("Fill" , &curr_poly_mode, 0); ImGui::SameLine();
                    ImGui::RadioButton("Line" , &curr_poly_mode, 1); ImGui::SameLine();
                    ImGui::RadioButton("Point", &curr_poly_mode, 2);

                    switch (curr_poly_mode)
                    {
                        case 0:
                            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                            break;
                        case 1:
                            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                            break;
                        case 2:
                            glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
                            break;
                    }

                    static int winding_order = 0;
                    ImGui::Text("Winding Order:"); ImGui::SameLine();
                    ImGui::RadioButton("CCW", &winding_order, 0); ImGui::SameLine();
                    ImGui::RadioButton("CW" , &winding_order, 1); 

                    switch (winding_order)
                    {
                        case 0:
                            glFrontFace(GL_CCW);
                            break;
                        case 1:
                            glFrontFace(GL_CW);
                            break;
                    }
                }

            }
            ImGui::End();
        }

        render_eof();
    }
}