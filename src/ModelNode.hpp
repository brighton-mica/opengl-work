#ifndef MICA_MODEL_NODE
#define MICA_MODEL_NODE

#include "Mesh.hpp"
#include "CullInfo.hpp"
#include "BoundingBox.hpp"

#include <vector>
#include <string>

class ModelNode
{
private:
  const std::string name;

  Mesh mesh;
  std::vector<ModelNode> children_list;

  GLuint vbo_handle = -1;
  GLuint ebo_handle = -1;

  // TODO - bounding box
  BBox bbox;
  glm::mat4 transformation;
public:
  ModelNode(const std::string _name)
    : name { std::move(_name) }
    , transformation { glm::mat4 { 1.0f } }
    , bbox { }
    , mesh { }
  {}

  void cull(CullInfo& cull_info) const;

  void add_mesh(Mesh _mesh) { mesh = std::move(_mesh); };
  void add_child(ModelNode node) { children_list.push_back(node); };

  void translate(const double x, const double y, const double z) 
  { transformation = glm::translate(transformation, glm::vec3(x, y, z)); }

  void update_mesh_transformation()
  { mesh.per_draw_data.model_matrix = transformation; }

  void set_bbox(BBox _bbox) { bbox = _bbox; };
  void set_vbo(GLuint handle) {  vbo_handle = handle; }
  void set_ebo(GLuint handle) {  ebo_handle = handle; }

  void destroy()
  {
    if (mesh.valid())
      mesh.destroy();

    glDeleteBuffers(1, &vbo_handle);
    glDeleteBuffers(1, &ebo_handle);
  }

  const BBox& get_bbox() const { return bbox; }
  std::string get_name() const { return name; }
};

#endif // MICA_MODEL_NODE