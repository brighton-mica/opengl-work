#ifndef SHADER_PROGRAM_HPP
#define SHADER_PROGRAM_HPP

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>

class ShaderProgram
{
private:
   GLuint m_shader_program_handle;
public:
   ShaderProgram(const std::string& vertex_shader_path, const std::string& fragment_shader_path, const std::string program_name); 

   void use() const { glUseProgram(m_shader_program_handle); }

   void set_uni_vec2(const std::string& uni_name, const glm::vec2& vec2) const
   { glUniform2fv(glGetUniformLocation(m_shader_program_handle, uni_name.c_str()), 1, &(vec2[0])); }

   void set_uni_vec3(const std::string& uni_name, const glm::vec3& vec3) const
   { glUniform3fv(glGetUniformLocation(m_shader_program_handle, uni_name.c_str()), 1, &(vec3[0])); }

   void set_uni_mat4(const std::string& uni_name, const glm::mat4& mat4) const
   { glUniformMatrix4fv(glGetUniformLocation(m_shader_program_handle, uni_name.c_str()), 1, GL_FALSE, glm::value_ptr(mat4)); }

   void set_uni_int(const std::string& uni_name, const GLint i) const
   { glUniform1i(glGetUniformLocation(m_shader_program_handle, uni_name.c_str()), i); }

   void set_uni_float(const std::string& uni_name, const GLfloat f) const
   { glUniform1f(glGetUniformLocation(m_shader_program_handle, uni_name.c_str()), f); }
};

#endif // SHADER_PROGRAM_HPP