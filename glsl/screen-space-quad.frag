#version 450

in vec2 v_uv;

void main()
{
    vec4 out_color = 
        (v_uv.x > 0.495 && v_uv.x < 0.505) 
        && (v_uv.y > 0.495 && v_uv.y < 0.505) 
        ? vec4(1.0f, 0.0f, 0.0f, 1.0f)
        : vec4(0.0f, 0.0f, 0.0f, 0.0f);

    if (out_color.a == 0.0)
        discard;
    
    gl_FragColor = out_color;
}