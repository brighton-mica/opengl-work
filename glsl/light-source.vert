#version 450 core

layout (location = 0) in vec3 a_pos;
// layout (location = 1) in vec3 a_norm;
// layout (location = 2) in vec2 a_texcoord;

uniform mat4 u_model_mat;
uniform mat4 u_view_mat;
uniform mat4 u_proj_mat;

void main()
{
    gl_Position = u_proj_mat * u_view_mat * u_model_mat * vec4(a_pos, 1.0f);
}
