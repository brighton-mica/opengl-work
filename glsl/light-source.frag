#version 450 core

in vec2 v_screen_coord;

out vec4 out_frag_color;

void main()
{
    out_frag_color = vec4(0.0, 1.0, 1.0, 1.0);
}